

/* Please ❤ this if you like it! */



(function($) { "use strict";
		
	$(document).ready(function(){"use strict";
	
		//Scroll back to top
		
		var progressPath = document.querySelector('.progress-wrap path');
		var pathLength = progressPath.getTotalLength();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'none';
		progressPath.style.strokeDasharray = pathLength + ' ' + pathLength;
		progressPath.style.strokeDashoffset = pathLength;
		progressPath.getBoundingClientRect();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';		
		var updateProgress = function () {
			var scroll = $(window).scrollTop();
			var height = $(document).height() - $(window).height();
			var progress = pathLength - (scroll * pathLength / height);
			progressPath.style.strokeDashoffset = progress;
		}
		updateProgress();
		$(window).scroll(updateProgress);	
		var offset = 50;
		var duration = 550;
		jQuery(window).on('scroll', function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.progress-wrap').addClass('active-progress');
			} else {
				jQuery('.progress-wrap').removeClass('active-progress');
			}
		});				
		jQuery('.progress-wrap').on('click', function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})
		
		
	});


	// Inquiry form
	$(document).ready(function() {
		$('#inquiryForm').submit(function (e) {
			var name = $('#inputName')
			var surname = $('#inputSurname')
			var email = $('#inputEmail')
			var cell = $('#inputCell')
			//var interest = $('#eventInterest')
			//var note = $('#stateCanger')
			let hrefURL = "https://velocityclub.typeform.com/to/KTiw6J?source=website&email=" + email.val() + "&name=" + name.val() + "&surname=" + surname.val() + "&cellphone=" + cell.val() + "&event_interest=false";

			//console.log(name.val());

			if (name.val() == "" || surname.val() == "" || email.val() == "" || cell.val() == "") {
				$('#errorMsg').fadeToggle(400);
				return false;
			} else {
				$.ajax({
					method: 'POST',
					url: 'https://formspree.io/xbjgbdkk',
					data: $('#inquiryForm').serialize(),
					datatype: 'json',
					beforeSend: function(){
						$("#loader").show();
					},
					complete: function (data) {
						$("#loader").hide();
						$('#errorMsg').hide();
						$('#successMsg').fadeToggle(400);
						window.location.href = hrefURL;
					},
					error: function(data) {
						$("#loader").hide();
						$('#successMsg').hide();
						$('#errorMsg').fadeToggle(400);
						window.location.href = hrefURL;
					}
				});
				e.preventDefault();
				$(this).get(0).reset();
				//$('#stateChanger').hide();
				// $('#successMsg').fadeToggle(400);
			}
		});

		$('#successMsg, #errorMsg').click(function() {
			$(this).hide();
		})
	})

	// EVENTS Lead form
	$(document).ready(function() {
		
		$('#eventLeadForm').submit(function(e) {
			var name = $('#inputNameEv')
			var surname = $('#inputSurnameEv')
			var email = $('#inputEmailEv')
			var cell = $('#inputCellphoneEv')
			//var interest = $('#eventInterestEv')
			let hrefURL = "https://velocityclub.typeform.com/to/KTiw6J?source=website&email=" + email.val() + "&name=" + name.val() + "&surname=" + surname.val() + "&cellphone=" + cell.val() + "&event_interest=true";

			//console.log(name);
		
			if(name.val() == "" || surname.val() == "" || email.val() == "" || cell.val() == "") {
				$('#errorMsgEv').fadeToggle(400);
				return false;
			}
			else {
				$.ajax({
					method: 'POST',
					url: 'https://formspree.io/xbjgbdkk',
					data: $('#eventLeadForm').serialize(),
					datatype: 'json',
					beforeSend: function(){
						$("#loaderEv").show();
					},
					complete: function (data) {
						$("#loaderEv").hide();
						$('#errorMsgEv').hide();
						$('#successMsgEv').fadeToggle(400);
						window.location.href = hrefURL;
					},
					error: function(data) {
						$("#loader").hide();
						$('#successMsgEv').hide();
						$('#errorMsgEv').fadeToggle(400);
					    window.location.href = hrefURL;
					}
				});
				e.preventDefault();
				$(this).get(0).reset();
				//$('#eventLeadForm').hide();
				//$('#successMsgEv').fadeToggle(400);
			}
		});
	

		$('#successMsgEv, #errorMsgEv').click(function() {
			$(this).hide();
		})
	})

    /*==============================================================
  		video magnific popup
  	==============================================================*/
	$(document).ready(function() {
        $('.popup-youtube').magnificPopup({
            disableOn: 0,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: "auto",
            closeBtnInside: false
        });
	})

    /*==============================================================
  		scroll to
  	==============================================================*/
    $(document).ready(function() {
        $("a[href^='#']").click(function(e) {
            e.preventDefault();

            var position = $($(this).attr("href")).offset().top;

            $("body, html").animate({
                scrollTop: position
            }, 500/* speed */, 'linear' );
        });
    })


})(jQuery);