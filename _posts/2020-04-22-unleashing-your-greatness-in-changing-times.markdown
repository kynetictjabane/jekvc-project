---
layout: post
title:  "Unleashing your greatness in changing times"
date:   2020-04-22 15:32:14 -0300
duration: 23
pullquote: "Embrace who you are and who you can be."
feature_img_mobile: buhle-mobile@2x.jpg
feature_img_desktop: buhle-desktop@2x.jpg
video_url: "https://www.youtube.com/watch?v=MUpJXxuYHIU"
---

These challenging times gives us the opportunity to find out what we’re capable of, develop our strengths and to access aspects of ourselves we didn’t know existed.

How have you remained optimistic about reaching your goals during South Africa’s lockdown?

**Buhle Dlamini** is an international speaker who has worked with iconic leaders like NelsonMandela and Desmond Tutu. Born and raised in a rural Zulu village in South Africa, he’s gone on to become an award-winning entrepreneur. Today he runs his own training and development company, Young and Able, who work with businesses to develop entrepreneurship, in their own companies and throughout South Africa. [https://www.buhledlamini.com/](https://www.buhledlamini.com/)