---
layout: post
title:  "New Post on Jekyll Demo"
date:   2020-06-22 08:00:00 -0300
venue: "Pretoria East"
duration: 2
pullquote: "Testing teh capabilities of Jekyll"
feature_img_url: "https://retail.momentum.co.za/images/homepage-banner/carousel/multiply/multiply-tablet.jpg"
---

Financial freedom is the ultimate goal, but it takes planning and effort to achieve.

What does a fulfilling life of financial freedom look like for you?