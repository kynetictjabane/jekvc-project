---
layout: post
title:  "How to rise above your current financial situation"
date:   2020-02-27 15:32:14 -0300
venue: "clico boutique hotel"
duration: 2
pullquote: "Tonight was about taking my first step to financial freedom."
feature_img_mobile: video-mobile@2x.jpg
feature_img_desktop: video-desktop@2x.jpg
video_url: "https://www.youtube.com/watch?v=YOTbgCmTWwc"
---

Financial freedom is the ultimate goal, but it takes planning and effort to achieve.

What does a fulfilling life of financial freedom look like for you?