---
layout: post
title:  "Github Post Test"
date:   2020-06-22 18:00:00 -0400
venue: "Muldersdrift"
duration: 2
pullquote: "Testing gitlab IDE post"
feature_img_url: "https://retail.momentum.co.za/images/homepage-banner/carousel/multiply/multiply-tablet.jpg"
---

This is a test page created in gitlab ide. Financial freedom is the ultimate goal, but it takes planning and effort to achieve.

What does a fulfilling life of financial freedom look like for you?

